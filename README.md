# Assignment4Trainers

Pokemon trainer website built in Angular. Has login functionality and a catalogue of 100 available pokemon which the user can collect, as well as a page to see collected pokemon and remove them from the collection again.

## Component tree

Link to our component tree, made with Figma: https://www.figma.com/file/Kkv62KL1bOCfLNMFUKlnP2/Assigenment-4-component-tree?node-id=0%3A1

## Setup
Run `npm install --global @angular/cli` to get the angular CLI that can serve up the page.

## Serving the page
NOTE: on current version of main, start on the /catalogue route.

Run `npm install` in the root folder of the project to install dependencies.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Dependencies

npm,
angular browsermodule,
angular routermodule,
angular httpclientmodule
