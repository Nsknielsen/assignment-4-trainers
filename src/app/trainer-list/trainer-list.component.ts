import { Component, OnInit } from '@angular/core';
import { Trainer } from '../models/trainer.model';
import { Pokemon } from '../services/models/pokemon.model';
import { User } from '../services/models/user.model';
import { TrainersService } from '../services/Trainers.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-trainer-list',
  templateUrl: './trainer-list.component.html',
  styleUrls: ['./trainer-list.component.css'],
})
export class TrainerListComponent implements OnInit {
  constructor(private readonly trainerService: TrainersService,
    private readonly userService: UserService) {}

  ngOnInit(): void {
    this.trainerService.fetchTrainers();
  }

  get user(): User {
    return this.userService.user()
  }

  pokemonArray: Pokemon[] | null = this.user.pokemon

  // get is a way to expose variables to the template
  get trainers(): Trainer[] {
    return this.trainerService.Trainers()
  }
}
