import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginPage } from './login/login.page';
import { TrainerPage } from './trainer/trainer.page';
import { CataloguePage } from './catalogue/catalogue.page';

import { CataloguePokemonListComponent } from './catalogue-pokemon-list/catalogue-pokemon-list.component';
import { PokemonListItemComponent } from './pokemon-list-item/pokemon-list-item.component';

import { AppRoutingModule } from './app-routing.module';
import { TrainerListComponent } from './trainer-list/trainer-list.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    CataloguePage,
    CataloguePokemonListComponent,
    PokemonListItemComponent,
    TrainerListComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
