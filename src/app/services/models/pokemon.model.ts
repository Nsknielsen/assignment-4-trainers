//data structure that fits the return value of Pokeapi
export interface Pokemon {
    name: string;
    url: string;
}