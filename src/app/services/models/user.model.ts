import { Pokemon } from "./pokemon.model";

//data structure for representing a user as uploaded to userAPI
export interface User{
    username: string;
    pokemon: Pokemon[] | null
}