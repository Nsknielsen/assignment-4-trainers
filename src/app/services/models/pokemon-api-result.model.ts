import { Pokemon } from "./pokemon.model";

//data type interface for handling the output of Pokeapi
export interface PokemonApiResult{
    'count': Number,
    'next': String | null,
    'previous': String | null,
    'results': Pokemon[]
}