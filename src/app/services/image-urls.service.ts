import { Injectable } from "@angular/core";
import { Pokemon } from "./models/pokemon.model";
import { POKEMON_IMAGES_BASE_URL } from "../const/api-info";
import { SINGLE_DIGIT_REGEX } from "../const/regex";

//globally available singleton service for getting pokemon images off github. used as sub-service for get-pokemon-service.
@Injectable({
    providedIn: 'root'
})
export class ImageUrlsService {
    //private array of pokemon image urls
    private _imageUrls: string[] = []

    //generate image urls by extracting pokemon id from pokemon url
    public generateUrls(pokemonArray: Pokemon[] | null): void {
        if (pokemonArray !== null){
            for (let index = 0; index < pokemonArray.length; index ++){
                let pokemonId = this.findPokemonId(pokemonArray[index])
                this._imageUrls.push(`${POKEMON_IMAGES_BASE_URL}/${pokemonId}.png`)
            }
        }
    }

    //extracts pokemon Id from url for use in image url generation
    private findPokemonId(pokemonObject: Pokemon): string {
        let pokemonId: string = ''
        const slicedString: string = pokemonObject.url.slice(-4)
        for (let character of slicedString){
            if(character.match(SINGLE_DIGIT_REGEX)){
                pokemonId += character
            }
        }
        return pokemonId
    }

    //exposes imageUrls
    public imageUrls(): string[]{
        return this._imageUrls
    }
}