import { HttpHeaders } from "@angular/common/http";
import { TRAINER_API_KEY } from "../const/api-info";

export const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      Authorization: TRAINER_API_KEY
    })
  };