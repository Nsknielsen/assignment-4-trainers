import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { TRAINER_API_BASE_URL, TRAINER_API_KEY } from "../const/api-info";
import { Pokemon } from "./models/pokemon.model";
import { User } from "./models/user.model";

@Injectable({
    providedIn: 'root'
})
export class TrainerApiService{

    constructor(private readonly http: HttpClient){}

    private _userObject: string | null = null

    //create http headers
    private createHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': TRAINER_API_KEY,
          })
    }

    //simple test get first before config to check if connecitng to api
    public getUser(userId: Number){
        const promise = this.http.get(`${TRAINER_API_BASE_URL}/${userId}`).toPromise()
        console.log(promise)
        promise.then((userData) => {
            this._userObject = JSON.stringify(userData)
            console.log('user', this._userObject)

        }).catch((error) => {
            console.log('promise rejected with error ' + JSON.stringify(error))
        })
    }

    //post a new user to api
    public patchUser(user: User){
        const headers = this.createHeaders()
        const promise = this.http.patch(`${TRAINER_API_BASE_URL}/1`, user, {
            headers
        }).toPromise().catch((error) => {
            console.log('promise rejected with error' + JSON.stringify(error))
        })
    }
   

}