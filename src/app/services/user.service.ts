import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { STORAGE_KEY_USER } from "../const/storage-keys";
import { storageRead, storageSave } from "../storage";
import { Pokemon } from "./models/pokemon.model";
import { User } from "./models/user.model";
import { TrainerApiService } from "./trainer-api-service";

//globally available singleton service for accessing the state of a user
@Injectable({
    providedIn: 'root'
})
export class UserService {
    //private user object 
    private _user:  User = (storageRead(STORAGE_KEY_USER) !== null ? storageRead(STORAGE_KEY_USER) : {})

    //user getter
    public user(): User{
        return this._user
    }

    constructor(private readonly trainerApiService: TrainerApiService){}

    //user's pokemon array getter
    public collectedPokemon(): Pokemon[] | null{
        return this._user.pokemon
    }

    //user setter
    public setUser(user: User): void {
        this._user = user
        storageSave(STORAGE_KEY_USER, this._user)
        this.trainerApiService.patchUser(this._user)
    }


    //adds a new pokemon to the user's collection and calls setter to update user in browser storage
    public addCollectedPokemon(newPokemon: Pokemon) {
        if (this._user.pokemon !== null){
            this._user?.pokemon.push(newPokemon)
    //NOTE this code might be superfluous when actual logging in is implemented
        const userObject: User = {
            username: this._user.username,
            pokemon: this._user.pokemon
        }
        this.setUser(userObject)
        }
    }

    public removeCollectedPokemon(removedPokemon: Pokemon){
        if (this._user.pokemon !== null){
            this._user.pokemon = this._user.pokemon.filter(item => {
                return item.name !== removedPokemon.name
            })
        }
        this.setUser(this._user)
        console.log(this._user.pokemon)
    }

    //checks if a user has already collected a given pokemon
    public checkIfCollected(checkPokemon: Pokemon | null): boolean {
        let isCollected = false
        if (this._user.pokemon !== null && checkPokemon !== null && this._user.pokemon !== undefined) {
            for (let existingPokemon of this._user.pokemon) {
                if (checkPokemon.name === existingPokemon.name) {
                    isCollected = true
                    break
                }
            }
        }
        return isCollected
    }
}