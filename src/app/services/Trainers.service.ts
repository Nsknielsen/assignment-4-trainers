import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Trainer } from '../models/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class TrainersService {
  //When the object(?) is marked with private, we cannot access the property outside the class.
  //It is not marked public, because any file that can read this service (where it is injected) can also alter the contacts.
  // This means contacts can be altered in many places, which can be a mess if we need to debug (Because we would have to look up everywhere, where this variable has been changed.)
  private trainers: Trainer[] = []
  private error1: string =''

  constructor(private readonly http: HttpClient) {}

  public fetchTrainers(): void {
    this.http.get<Trainer[]>('https://jah-noroff-api.herokuapp.com/trainers')
    .subscribe((trainers: Trainer[]) => {
        this.trainers = trainers
    }, (error:HttpErrorResponse) => {
        this.error1 = error.message

    })
  }
  public Trainers(): Trainer[] {
    return this.trainers;
  }
  public error(): string {
    return this.error1;
  }
}
