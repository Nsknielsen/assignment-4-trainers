import { Injectable } from "@angular/core";
import { Pokemon } from "./models/pokemon.model";
import { PokemonApiResult } from "./models/pokemon-api-result.model";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { POKE_API_BASE_URL } from "../const/api-info";
import { ImageUrlsService } from "./image-urls.service";
import { storageRead, storageSave } from "../storage";
import { STORAGE_KEY_ALL_POKEMON } from "../const/storage-keys";

//globally available singleton service for fetching pokemon from PokeAPI
@Injectable({
    providedIn: 'root'
})
export class GetPokemonService{
    //private result object including next url for use in optional pagination.
    private _resultObject: PokemonApiResult | null = null

    //array of pokemon objects, exposed.
    private _pokemonArray: Pokemon[] | null = storageRead(STORAGE_KEY_ALL_POKEMON)

    private _error: string = ''

    constructor(private readonly http: HttpClient,
        private readonly imageUrlsService: ImageUrlsService){}

    //gets 100 pokemon from the api on default, and updates session storage with these pokemon for future use
    public fetchPokemon(): void {
        if (this._pokemonArray === null){
            this.http.get<PokemonApiResult>(`${POKE_API_BASE_URL}?limit=100&offset=0`)
            .subscribe((pokemonApiResult) => {
            this._resultObject = pokemonApiResult
            this._pokemonArray = this._resultObject.results
            this.imageUrlsService.generateUrls(this._pokemonArray)
            for (let i = 0; i < this._pokemonArray.length; i++){
                 this._pokemonArray[i].url = this.imageUrlsService.imageUrls()[i]
            }
            storageSave(STORAGE_KEY_ALL_POKEMON, this._pokemonArray)
            }, (error: HttpErrorResponse) => {
            this._error = error.message
            })
        }  
    }

    //pokemon array getter
    public pokemon(): Pokemon[] | null{
        return this._pokemonArray
    }

    //error getter
    public error(): string{
        return this._error
    }
}