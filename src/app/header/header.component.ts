import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private readonly userService: UserService) { }

  //only displays navbar while user is logged in
  public isLoggedIn(): boolean{
    if (this.userService.user().username !== undefined){
      return true
    }
    return false
  }

  ngOnInit(): void {
  }

}
