import { Component, OnInit } from '@angular/core';
import { Pokemon } from "../services/models/pokemon.model";
import {GetPokemonService}  from '../services/poke-api.service';
import { TrainerApiService } from '../services/trainer-api-service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-catalogue-pokemon-list',
  templateUrl: './catalogue-pokemon-list.component.html',
  styleUrls: ['./catalogue-pokemon-list.component.css']
})
export class CataloguePokemonListComponent implements OnInit {

  constructor(private readonly getPokemonService: GetPokemonService,
    private readonly userService: UserService) { }

    //expose list of all pokemon to the html
  get pokemonArray(): Pokemon[] | null{
    return this.getPokemonService.pokemon()
  }

  //TEST login
  public testLogin(): void {
    this.userService.setUser({username: 'testuser', pokemon: []})
  }
 
  //fetch pokeapi data on initialization
  ngOnInit(): void {
    this.getPokemonService.fetchPokemon()
  }

}
