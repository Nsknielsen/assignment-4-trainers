import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../services/models/pokemon.model';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent {

  constructor(private readonly userService: UserService) { }

  //get one pokemon from parent component to display
  @Input() pokemon: Pokemon | null = null

  //get a type to dictate which buttons to show. Either catalogue-page or trainer-page
  @Input() type: string = ''


  //Array used so the html can access collectedPokemon and disable button if this one is already collected
  pokemonArray: Pokemon[] | null = this.userService.collectedPokemon()
  

  //handle adding the pokemon of this element to the user's collection in state and local storage (used if this is a catalogue sub-component)
  public addToCollection(): void {
    if (this.pokemon !== null && this.userService.user() !== null && this.userService.collectedPokemon() !== undefined){
      console.log('has user, in list item')
      if (!this.checkIfCollected()){
        this.userService.addCollectedPokemon(this.pokemon)
      }
    }
  }

  //handles removing this pokemon from collection (used if this is a trainer list sub-component)
  public removeFromCollection(): void {
    if (this.pokemon !== null && this.userService.user() !== null){
      this.userService.removeCollectedPokemon(this.pokemon)
    }
    console.log('removed', this.pokemon)
    
  }

  //gives html indirect access to the private collectedpokemonservice's function for seeing if a pokemon has been collected
  public checkIfCollected(): boolean {
    return this.userService.checkIfCollected(this.pokemon)
  }
}
