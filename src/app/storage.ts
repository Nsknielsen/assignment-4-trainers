//add, delete and get user in local browser storage

import { Pokemon } from "./services/models/pokemon.model"
import { User } from "./services/models/user.model"

//can save both userobj and catalogue array of available pokemon with same function
export const storageSave = (key: string, value: User | Pokemon[] | null) => {

    if (!value){
        throw new Error('storageSave: no value provided for ' + key)
    }

    sessionStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = (key: string) => {
   
    const data = sessionStorage.getItem(key)
    if (data){
        return JSON.parse(data)
    }

    return null
}

export const storageDelete = (key: string) => {
    sessionStorage.removeItem(key)
}